package org.manathome.dawn;

import org.manathome.dawn.processor.JobProcessor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * minimal ui.
 */
@Controller
public class DawnController {

  private final JobProcessor processor;

  public DawnController(JobProcessor processor) {
    this.processor = processor;
  }

  @RequestMapping(value = "/", produces = "text/plain")
  public @ResponseBody String index() {
    return "dawn works on " + processor.getJobDirectoryPath().toAbsolutePath() + ".\n";
  }

}
