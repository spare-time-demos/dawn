package org.manathome.dawn;

import org.manathome.dawn.util.JobConfigurationProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * spring entry point of dawn application (main).
 *
 * @author manfred
 * @since 2024-02-28
 **/
@SpringBootApplication
@EnableConfigurationProperties(JobConfigurationProperties.class)
@EnableScheduling
public class DawnApplication {

  /**
   * main entry point.
   **/
  public static void main(String[] args) {
    SpringApplication.run(DawnApplication.class, args);
  }

}
