package org.manathome.dawn;

import org.manathome.dawn.job.JobProcessingState;
import org.manathome.dawn.job.JobQueue;
import org.manathome.dawn.processor.JobProcessor;
import org.manathome.dawn.util.Require;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * background task to poll jira issues for new work.
 **/
@Component
public class DawnJobProcessorTask {

  private static final Logger log = LoggerFactory.getLogger(DawnJobProcessorTask.class);

  private static final long ONE_SECOND_DELAY = 1000;

  private final JobProcessor processor;
  private long pollCount = 0;

  private final JobQueue jobQueue;

  public DawnJobProcessorTask(JobProcessor processor) {
    this.processor = Require.notNull(processor, "processor");
    this.jobQueue = new JobQueue();
  }

  /** periodically poll jira for new issues and process them. */
  @Scheduled(fixedDelay = ONE_SECOND_DELAY)
  public boolean pollAndProcessJiraIssues() {

    log.debug("next poll " + pollCount++ + " for jira issues");
    processor.retrieveOpenCommissions().forEach(j -> jobQueue.commissionJobIfMissing(j));

    var jobsToProcess = jobQueue.getJobsOfState(JobProcessingState.commissioned).toList();

    if (!jobsToProcess.isEmpty()) {
      log.debug("process " + jobsToProcess.size() + " jira issues as commissioned jobs");
      jobsToProcess.forEach(processor::commission);
      return true;
    }
    return false;
  }
}
