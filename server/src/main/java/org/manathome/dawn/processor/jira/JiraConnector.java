package org.manathome.dawn.processor.jira;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.Gauge;
import io.micrometer.core.instrument.MeterRegistry;
import org.manathome.dawn.util.JobConfigurationProperties;
import org.manathome.dawn.util.Require;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClient;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Base64;
import java.util.stream.Stream;

/**
 * get in contact with JIRA Tickets.
 * <a href="https://developer.atlassian.com/server/jira/platform/jira-rest-api-examples/#searching-for-issues-examples">
 * api examples</a>.
 *
 * @see JobConfigurationProperties#jiraApiUrl()
 **/
@Service
public class JiraConnector {

  private static final Logger logger = LoggerFactory.getLogger(JiraConnector.class);

  private final JobConfigurationProperties properties;

  private final RestClient restClient;

  private final Counter transitionCounter;

  private final Counter searchCounter;

  private int lastJiraReadResult;


  /** .ctor. */
  public JiraConnector(final JobConfigurationProperties properties, final MeterRegistry meterRegistry) {

    this.properties = Require.notNull(properties, "properties");
    logger.info("jira: api initialized on: " + this.properties.jiraApiUrl() + " as " + this.properties.jiraApiUser());

    restClient = Require.notNull(RestClient.builder()
      .defaultHeader(
        HttpHeaders.AUTHORIZATION,
        encodeBasic(this.properties.jiraApiUser(), this.properties.jiraApiToken()))
      .defaultHeader("User-Agent", "dawn jira job retriever")
      .build(), "restClient");

    transitionCounter = Counter
      .builder("jira.transition.count")
      .description("how many jobs where transistioned in jira")
      .register(meterRegistry);

    searchCounter = Counter
      .builder("jira.search.issues.count")
      .description("how many jobs for scheduling where found in jira")
      .register(meterRegistry);

    Gauge
      .builder("jira.read.success", this, t -> t.lastJiraReadResult)
      .description("last read access to jira status")
      .register(meterRegistry);

  }

  /**
   * create a valid http basic auth user+password string.
   */
  private String encodeBasic(final String username, final String password) {

    var token = Require.notEmptyOrBlank(password, "password");

    Require.isTrue(!token.contains("JIRA_API_TOKEN"), "JIRA_API_TOKEN not configured for jira access.");

    return "Basic "
      + Base64.getEncoder().encodeToString((
      Require.notEmptyOrBlank(username, "username")
        + ":"
        + token).getBytes(StandardCharsets.UTF_8));
  }

  /**
   * retrieve jira tickets from JIRA (open issues, that need to be run).
   **/
  public Stream<JiraIssue> getOpenIssues() {

    final var searchInProgressUrl = Require.notEmptyOrBlank(properties.jiraApiUrl(), "jiraApiUrl")
        + properties.jiraApiSearchTerm();

    logger.debug("jira call " + searchInProgressUrl);

    try {
      var result = Require.notNull(restClient
        .get()
        .uri(searchInProgressUrl)
        .accept(MediaType.APPLICATION_JSON)
        .retrieve()
        .body(JiraQueryResult.class), "result");

      lastJiraReadResult = 1;
      searchCounter.increment(result.total());
      logger.info("rest response with issue count: " + result.total());

      return Arrays.stream(result.issues());

    } catch (Exception ex) {
      lastJiraReadResult = -1;
      logger.error("error reading issues from jira " + ex.getMessage(), ex);
      throw ex;
    }
    // TODO @man missing error code handling.

  }

  /**
   * retrieve a jira ticket with given id from JIRA.
   *
   * @param id jira ticket id.
   **/
  public @NonNull JiraIssue getIssue(final @NonNull String id) {

    final var getIssueUrl = Require.notEmptyOrBlank(properties.jiraApiUrl(), "getIssueUrl") + "issue/" + id;

    logger.debug("jira call " + getIssueUrl);

    var result = restClient
      .get()
      .uri(getIssueUrl)
      .accept(MediaType.APPLICATION_JSON)
      .retrieve()
      .body(JiraIssue.class);

    logger.info("rest response with issue: " + result);

    return Require.notNull(result, "result");
  }

  /**
   * change state of jira ticket with given id in JIRA.
   *
   * @param id               jira ticket id.
   * @param targetTransition new state to change into.
   **/
  public void changeJiraIssueState(final @NonNull String id, final @NonNull String targetTransition) {

    final var getChangeIssueUrl = Require.notEmptyOrBlank(properties.jiraApiUrl(), "getIssueUrl")
      + "issue/" + id
      + "/transitions";

    var jsonChangeBody = """
      {"transition": {"id":"[id]"}}
      """;

    jsonChangeBody = jsonChangeBody.replace("[id]", targetTransition);

    logger.debug("jira call " + getChangeIssueUrl + " to " + targetTransition);

    var result = restClient
      .post()
      .uri(getChangeIssueUrl)
      .contentType(MediaType.APPLICATION_JSON)
      .body(jsonChangeBody)
      .accept(MediaType.APPLICATION_JSON)
      .retrieve()
      .toBodilessEntity();

    transitionCounter.increment();
    logger.info("rest response with issue: " + result);
  }

  /**
   * add comment to jira ticket with given id.
   *
   * @param id      jira ticket id.
   * @param comment new comment to add.
   **/
  public void addJiraIssueComment(final @NonNull String id, final @NonNull String comment) {

    final var getChangeIssueUrl = properties.jiraApiUrl()
      + "issue/" + id
      + "/comment";

    var jsonChangeBody = """
      {"body": "[comment]"}
      """;

    var s = Require.notEmptyOrBlank(comment, "comment")
      .trim()
      .replace("\\", ".")
      .replace("\"", ".");

    jsonChangeBody = jsonChangeBody.replace("[comment]", s);

    logger.debug("jira call " + getChangeIssueUrl + " commenting");

    var result = restClient
      .post()
      .uri(getChangeIssueUrl)
      .contentType(MediaType.APPLICATION_JSON)
      .body(jsonChangeBody)
      .accept(MediaType.APPLICATION_JSON)
      .retrieve()
      .toBodilessEntity();

    logger.info("rest response with issue: " + result);
  }

}
