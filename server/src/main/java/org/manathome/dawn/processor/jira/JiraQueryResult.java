package org.manathome.dawn.processor.jira;

/** jira json structure.
 *
 * @param total count off issues.
 * @param issues list of found jira issues.
 **/
public record JiraQueryResult(long total, JiraIssue[] issues) {
}
