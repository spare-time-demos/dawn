package org.manathome.dawn.processor.jira;

/** jira json structure. **/
public record JiraIssue(String id, String key, JiraFields fields) {
}
