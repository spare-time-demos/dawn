package org.manathome.dawn.processor.jira;

import java.util.Calendar;

/**
 * jira json structure.
 **/
public record JiraFields(Calendar created,

                         String description,

                         String summary,

                         JiraStatus status,

                         JiraCommentList comment

) {
}
