package org.manathome.dawn.processor;

import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import org.manathome.dawn.job.JobCommission;
import org.manathome.dawn.job.JobProcessingState;
import org.manathome.dawn.processor.jira.JiraConnector;
import org.manathome.dawn.util.AuditLog;
import org.manathome.dawn.util.JobConfigurationProperties;
import org.manathome.dawn.util.Require;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * work on jobs.
 **/
@Service
public class JobProcessor {

  public static final String JOB_SUFFIX = ".control-m.job";
  private static final Logger logger = LoggerFactory.getLogger(JobProcessor.class);
  private final JobConfigurationProperties properties;

  private final JiraConnector connector;

  private final Counter scheduledCounter;

  private final Counter skippedCounter;

  private final Counter errorCounter;

  /**
   * jira to control files logic.
   */
  public JobProcessor(
    JobConfigurationProperties properties,
    JiraConnector connector,
    MeterRegistry meterRegistry) {

    this.properties = properties;
    this.connector = connector;

    scheduledCounter = Counter
      .builder("job.scheduled.ok.count")
      .description("how many jobs where scheduled")
      .register(meterRegistry);

    skippedCounter = Counter
      .builder("job.scheduled.skipped.count")
      .description("how many jobs where skipped (control file already there")
      .register(meterRegistry);

    errorCounter = Counter
      .builder("job.scheduled.error.count")
      .description("how many jobs where not scheduled with error.")
      .register(meterRegistry);

    logger.info("processor initialized on path: "
      + this.properties.jobDirectoryPath()
      + "and jira "
      + this.properties.jiraApiUrl());
  }


  /**
   * directory to create job files in.
   **/
  public Path getJobDirectoryPath() {

    Path path = Paths.get(Require.notNull(properties.jobDirectoryPath(), "not job path configured"));
    Require.isTrue(Files.isDirectory(path), "not a directory " + path.toAbsolutePath());

    return path;
  }

  public Path getJobFilePath(final JobCommission jobCommission) {
    return getJobDirectoryPath().resolve(jobCommission.getId() + JOB_SUFFIX);
  }

  /**
   * retrieve new jobs to be run from jira.
   **/
  public List<JobCommission> retrieveOpenCommissions() {

    List<JobCommission> jcs = new ArrayList<>();

    final var issues = this.connector.getOpenIssues();

    issues.forEach(i ->
      jcs.add(new JobCommission(i.id(), i.key(), JobProcessingState.commissioned))
    );
    return jcs;
  }

  /**
   * send a job to be run (create control-m file).
   **/
  public boolean commission(final @NonNull JobCommission jobCommission) {
    Require.notNull(jobCommission, "no job given");
    Require.isTrue(jobCommission.getState() == JobProcessingState.commissioned, "job not commissioned");

    Path filePath = getJobFilePath(jobCommission);

    if (Files.exists(filePath)) {
      try {
        var content = Files.readString(filePath);
        if (!content.equals(jobCommission.getControlFileContent())) {
          logger.warn("already existing control file " + filePath + " with difference in content");
          errorCounter.increment();
          throw new IllegalStateException("existing control file "
            + filePath
            + " for "
            + jobCommission.getJob()
            + " differs in content");
        }
      } catch (IOException ioex) {

        logger.warn("already existing control file " + filePath + " that coul not be read");
        errorCounter.increment();
        throw new IllegalStateException(("existing control file "
          + filePath
          + " for "
          + jobCommission.getJob()
          + " could not be read. "
          + ioex.getMessage()));
      }

      logger.info("ignore job, already existing control file " + filePath);
      skippedCounter.increment();
      jobCommission.setState(JobProcessingState.scheduled);

      return false;

    } else {

      // write control-file
      Require.isTrue(!Files.exists(filePath), "already existing " + filePath);
      logger.info("creating control file " + filePath);

      try {
        Files.writeString(filePath, jobCommission.getControlFileContent());
      } catch (Exception ex) {
        logger.error("error creating control file " + filePath, ex);
        throw new IllegalStateException("file " + filePath + "could not be written: " + ex.getMessage(), ex);
      }

      scheduledCounter.increment();
      jobCommission.setState(JobProcessingState.scheduled);
      AuditLog.audit("scheduled job "
          + jobCommission.getJob()
          + " by " + jobCommission.getCommissionedBy()
          + " as " + filePath);

      connector.addJiraIssueComment(jobCommission.getId(), "scheduled as " + filePath);
      connector.changeJiraIssueState(jobCommission.getId(), properties.jiraTransitionRequestRun());

      AuditLog.audit("scheduled job "
        + jobCommission.getJob()
        + " by " + jobCommission.getCommissionedBy()
        + " completed in jira");

      return true;
    }
  }

  /**
   * cleanup all job control files.
   * <b>BE CAREFUL, TEST USE MOSTLY.</b>
   *
   * @return deleted file count.
   */
  public int cleanupControlFiles() {

    logger.info("cleanup directory " + getJobDirectoryPath());

    try {
      var deleteCount = 0;
      try (DirectoryStream<Path> stream = Files.newDirectoryStream(getJobDirectoryPath())) {
        for (Path path : stream) {
          if (!Files.isDirectory(path) && path.toString().endsWith(JOB_SUFFIX)) {

            logger.debug("cleanup of " + path);
            Files.delete(path);
            deleteCount++;
          }
        }
        return deleteCount;
      }
    } catch (Exception ex) {
      logger.error("error cleanup directory " + getJobDirectoryPath(), ex);
      throw new IllegalStateException("cleanup directory " + getJobDirectoryPath() + " failed. " + ex.getMessage(), ex);
    }
  }

}
