package org.manathome.dawn.processor.jira;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * all jira ticket comments.
 */
public record JiraCommentList(JiraComment[] comments) {

  @Override
  public String toString() {

    return comments == null
      ? "comments[null]"
      : "comments[" + Arrays.stream(comments).map(c -> c.body()).collect(Collectors.joining(",\n")) + "]";
  }
}
