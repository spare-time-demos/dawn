package org.manathome.dawn.processor.jira;

/**
 * single jira comment.
 */
public record JiraComment(String body) {

  @Override
  public String toString() {
    return body;
  }
}
