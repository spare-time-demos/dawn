package org.manathome.dawn.processor.jira;

/** jira json structure for status field. **/
public record JiraStatus(String name, String id) {

  public static final String STATUS_ID_IN_ARBEIT = "10001";
  public static final String STATUS_ID_FERTIG = "10002";
}
