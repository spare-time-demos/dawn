package org.manathome.dawn.job;

/**
 * job livecycle states.
 **/
public enum JobProcessingState {
  undefined,

  commissioned,

  scheduled,

  done
}
