package org.manathome.dawn.job;

import org.manathome.dawn.util.Require;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

/**
 * holds all jobs known to this app.
 */
public class JobQueue {

  /**
   * list of job in varying stages/states.
   **/
  private final List<JobCommission> commissionedJobs;

  /**
   * .ctor.
   */
  public JobQueue() {
    this.commissionedJobs = new ArrayList<JobCommission>();
  }

  /**
   * adds job to the list - only if not already in list.
   */
  public boolean commissionJobIfMissing(final JobCommission jobCommission) {
    Require.notNull(jobCommission, "added JobCommission");
    if (!this.commissionedJobs.stream().anyMatch(cj -> cj.equals(jobCommission))) {
      this.commissionedJobs.add(jobCommission);
      return true;
    }
    return false;
  }

  /**
   * get all jobs.
   */
  public Stream<JobCommission> getJobs() {
    return commissionedJobs.stream();
  }

  /**
   * get filtered jobs.
   */
  public Stream<JobCommission> getJobsOfState(final JobProcessingState state) {
    return commissionedJobs
      .stream()
      .filter(jc -> jc.getState().equals(state));
  }
}
