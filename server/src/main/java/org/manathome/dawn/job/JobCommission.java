package org.manathome.dawn.job;

import org.manathome.dawn.util.Require;
import org.springframework.lang.NonNull;

import java.time.LocalDateTime;

/**
 * a request to run a job.
 */
public class JobCommission {

  private final @NonNull LocalDateTime commissionedAt;
  private String id;
  private String commissionedBy;
  private String job;
  private JobProcessingState state;

  /**
   * .ctor.
   */
  public JobCommission() {
    this.commissionedAt = LocalDateTime.now();
    this.state = JobProcessingState.undefined;
  }

  /**
   * .ctor.
   */
  public JobCommission(final String id, final String job, JobProcessingState state) {
    this();
    this.id = Require.notNull(id, "id");
    this.state = Require.notNull(state, "state");
    this.job = Require.notNull(job, "job");
  }

  public String getId() {
    return id;
  }

  public String getCommissionedBy() {
    return commissionedBy;
  }

  public void setCommissionedBy(String commissionedBy) {
    this.commissionedBy = commissionedBy;
  }

  public LocalDateTime getCommissionedAt() {
    return commissionedAt;
  }

  public String getJob() {
    return job;
  }

  public void setJob(String job) {
    this.job = job;
  }

  public JobProcessingState getState() {
    return state;
  }

  public void setState(JobProcessingState state) {
    this.state = state;
  }

  @Override
  public int hashCode() {
    return this.id.hashCode();
  }

  @Override
  public boolean equals(Object obj) {
    return obj != null
      && getClass() == obj.getClass()
      && this.id.equals(((JobCommission) obj).getId());
  }

  @Override
  public String toString() {
    return super.toString();
  }

  /** job control file content. */
  public String getControlFileContent() {
    // TODO @mrj simulate clearquest logic for control-m !!
    return this.id + ": " + this.job + ": " + this.commissionedBy;
  }
}
