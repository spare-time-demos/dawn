package org.manathome.dawn.util;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * properties from application.properties.
 *
 * @param jobDirectoryPath where to create the job control files to feed control-m with.
 * @param jiraApiUrl       jira rest api url             = "https://manfromhome.atlassian.net/rest/api/2/
 * @param jiraApiSearchTerm jira search for open tickets = search?jql=status="in Progress"
 * @param jiraApiUser      jira user                     = "man.from.home@gmail.com"
 * @param jiraApiToken     jira api token                = "ATATT3x......."
 * @param jiraTransitionRequestRun jira internal state transition id to change into with running the job ("2")
 * @param jiraTransitionInProgress jira internal state transition id to set issue back to in progress ("21")
 **/
@ConfigurationProperties(prefix = "jobs")
public record JobConfigurationProperties(String jobDirectoryPath,

                                         String jiraApiUrl,

                                         String jiraApiSearchTerm,

                                         String jiraApiUser,

                                         String jiraApiToken,

                                         String jiraTransitionRequestRun,

                                         String jiraTransitionInProgress) {
}
