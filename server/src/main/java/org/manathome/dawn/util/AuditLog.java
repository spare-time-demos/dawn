package org.manathome.dawn.util;

import org.manathome.dawn.processor.JobProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** basic audt log helper. */
public final class AuditLog {
  private static final Logger logger = LoggerFactory.getLogger("AUDIT");

  public static void audit(final String msg) {
    logger.info(msg);
  }
}
