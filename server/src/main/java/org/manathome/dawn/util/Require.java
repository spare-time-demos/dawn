package org.manathome.dawn.util;

import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

/**
 * assertions in code.
 **/
public final class Require {
  private Require() {
  }

  /**
   * ensure object is not null.
   *
   * @param shouldNotBeNullObject the object to check
   * @param hintOnNull            hint given on null exception
   * @return the shouldNotBeNullObject, unchanged object
   * @throws NullPointerException if object is null
   **/
  public static <T> @NonNull T notNull(final @Nullable T shouldNotBeNullObject, final @Nullable String hintOnNull) {
    if (shouldNotBeNullObject == null) {
      throw new NullPointerException("Object is null: " + hintOnNull);
    }
    return shouldNotBeNullObject;
  }

  /**
   * ensure object is not null.
   *
   * @param shouldNotBeNullOrEmptyString the string to check
   * @param hintOnEmpty                  hint given on null exception
   * @return the unchanged string
   * @throws IllegalArgumentException if string is null or empty or blank
   **/
  public static @NonNull String notEmptyOrBlank(
      final @Nullable String shouldNotBeNullOrEmptyString,
      final @Nullable String hintOnEmpty) {

    if (shouldNotBeNullOrEmptyString == null || shouldNotBeNullOrEmptyString.trim().length() == 0) {
      throw new IllegalArgumentException("string is null or empty: " + hintOnEmpty);
    }
    return shouldNotBeNullOrEmptyString;
  }


  /**
   * ensure condition is true.
   *
   * @param trueCondition expected to be true
   * @param hintOnFalse   hint given on false
   * @throws IllegalStateException if condition is false.
   **/
  public static void isTrue(boolean trueCondition, final @Nullable String hintOnFalse) {
    if (!trueCondition) {
      throw new IllegalStateException("not expected state: " + hintOnFalse);
    }
  }
}
