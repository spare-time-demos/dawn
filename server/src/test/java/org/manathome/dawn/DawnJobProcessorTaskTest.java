package org.manathome.dawn;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.manathome.dawn.util.Require;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

/** test the whole process. */
@SpringBootTest
public class DawnJobProcessorTaskTest {

  @Autowired
  DawnJobProcessorTask task;

  /** avoid running background task. */
  @MockBean
  DawnJobProcessorTask stubbedOutTask;

  @Test
  @Tag("SystemTest")
  public void pollAndProcessJiraIssues() {
    var rc = Require.notNull(task, "task").pollAndProcessJiraIssues();
  }
}
