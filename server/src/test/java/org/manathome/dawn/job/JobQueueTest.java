package org.manathome.dawn.job;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * test.
 */
@Tag("UnitTest")
public class JobQueueTest {

  @Test
  public void addJobToQueue() {
    JobQueue jq = new JobQueue();

    var j = new JobCommission("t1", "job1", JobProcessingState.commissioned);
    assertTrue(jq.commissionJobIfMissing(j), "first add");
    assertFalse(jq.commissionJobIfMissing(j), "not twice");

    assertEquals(1, jq.getJobs().count());
  }

  @Test
  public void addIdenticalJobsToQueue() {
    JobQueue jq = new JobQueue();

    var j = new JobCommission("t1", "job1", JobProcessingState.commissioned);
    assertTrue(jq.commissionJobIfMissing(j), "first add");
    j = new JobCommission("t1", "job1-duplicate", JobProcessingState.commissioned);
    assertFalse(jq.commissionJobIfMissing(j), "not twice");

    assertEquals(1, jq.getJobs().count());
  }

  @Test
  public void addSeveralJobsToQueue() {
    JobQueue jq = new JobQueue();

    var j = new JobCommission("t1", "job1", JobProcessingState.commissioned);
    assertTrue(jq.commissionJobIfMissing(j), "first add");
    j = new JobCommission("t2", "job2", JobProcessingState.commissioned);
    assertTrue(jq.commissionJobIfMissing(j), "second twice");

    assertEquals(2, jq.getJobs().count(), "two total");
    assertEquals(2, jq.getJobsOfState(JobProcessingState.commissioned).count(), "two commissioned");
    assertEquals(0, jq.getJobsOfState(JobProcessingState.scheduled).count(), "two commissioned");
  }
}
