package org.manathome.dawn;

import org.junit.jupiter.api.Test;
import org.manathome.dawn.processor.JobProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * dummy application test.
 **/
@SpringBootTest
@ConfigurationPropertiesScan
public class DawnApplicationTests {

  @Autowired
  JobProcessor processor;

  /** avoid running background task. */
  @MockBean
  DawnJobProcessorTask stubbedOutTask;

  @Test
  void contextLoads() {
  }

  @Test
  void checkComponents() {
    assertNotNull(processor);
  }

}
