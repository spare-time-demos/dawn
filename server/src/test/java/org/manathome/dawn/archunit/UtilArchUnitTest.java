package org.manathome.dawn.archunit;

import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.lang.ArchRule;
import org.manathome.dawn.util.Require;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClass;
import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.theClass;

/** archunit.org tests. */
@AnalyzeClasses(packages = "org.manathome.dawn.util")
public class UtilArchUnitTest {

  @ArchTest
  static final ArchRule require_should_only_access_classes_in_util_itself =
    noClass(Require.class)
      .should().accessClassesThat().resideOutsideOfPackages("..util..", "java..");

}
