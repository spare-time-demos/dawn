package org.manathome.dawn.util;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * test the assertion.
 *
 */
@Tag("UnitTest")
public class RequireTest {

  @Test
  void isNullTest() {

    Exception exception = assertThrows(NullPointerException.class, () -> Require.notNull(null, "nothing was given"));
    assertTrue(exception.getMessage().contains("nothing was given"));
  }

  @Test
  void isNotNullTest() {

    var result = Require.notNull("notNullString", "nothing was given");
    assertEquals(result, "notNullString");
  }

  @Test
  void isTrueTest() {
    Require.isTrue(true, "nothing was given");
  }

  @Test
  void isNotTrueTest() {
    Exception exception = assertThrows(IllegalStateException.class, () -> Require.isTrue(false, "not met"));
    assertTrue(exception.getMessage().contains("not met"));
  }

}
