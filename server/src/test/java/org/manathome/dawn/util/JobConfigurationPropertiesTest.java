package org.manathome.dawn.util;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.manathome.dawn.DawnJobProcessorTask;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Test Config Load.
 */
@SpringBootTest
@ConfigurationPropertiesScan
@Tag("UnitTest")
// @EnableConfigurationProperties(JobConfigurationProperties.class)
public class JobConfigurationPropertiesTest {

  @Autowired
  public JobConfigurationProperties jobConfiguration;

  /** avoid running background task. */
  @MockBean
  DawnJobProcessorTask stubbedOutTask;

  @Test
  public void jobDirectoryIsSet() {
    assertNotNull(jobConfiguration, "job configuration initalized");
    assertTrue(jobConfiguration.jobDirectoryPath().length() > 2);
  }
}
