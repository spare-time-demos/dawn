package org.manathome.dawn.processor;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.manathome.dawn.DawnJobProcessorTask;
import org.manathome.dawn.job.JobCommission;
import org.manathome.dawn.job.JobProcessingState;
import org.manathome.dawn.util.JobConfigurationProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.nio.file.Files;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * test file creation.
 */
@Tag("SystemTest")
@SpringBootTest
public class JobProcessorTest {

  @Autowired
  JobProcessor processor;

  /** avoid running background task. */
  @MockBean
  DawnJobProcessorTask stubbedOutTask;

  @BeforeEach
  public void beforeEach() {
    processor.cleanupControlFiles();
  }

  @Test
  public void retrieveJobsFromJira() {

    final var ocs = processor.retrieveOpenCommissions();

    assertNotNull(ocs);
    assertTrue(!ocs.isEmpty(), "no jobs found");
  }
}
