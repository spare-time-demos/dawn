package org.manathome.dawn.processor.jira;

import org.junit.jupiter.api.Assumptions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.manathome.dawn.DawnJobProcessorTask;
import org.manathome.dawn.util.JobConfigurationProperties;
import org.manathome.dawn.util.Require;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.web.client.HttpClientErrorException;

import java.util.Arrays;
import java.util.Calendar;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;


/**
 * test jira access.
 */
@SpringBootTest
@Tag("SystemTest")
public class JiraConnectorTest {

  public static final String JIRA_ISSUE_ID_TEST = "JOB-2";

  public static final String JIRA_ISSUE_ID_TEST_NON_EXISTENT = "JOB-NOT-THERE-2";

  @Autowired
  JiraConnector connector;

  @Autowired
  JobConfigurationProperties properties;

  /** avoid running background task. */
  @MockBean
  DawnJobProcessorTask stubbedOutTask;

  @Test
  public void getTicketsFromJira() {

    final Calendar startDate = Calendar.getInstance();
    startDate.set(1999, 12, 31);

    final var issue = Require.notNull(connector.getIssue(JIRA_ISSUE_ID_TEST), "issue");

    if(!issue.fields().status().id().equals(JiraStatus.STATUS_ID_IN_ARBEIT)) {
      // ensure that at least this ticket is in correct state to be found.
      connector.changeJiraIssueState(JIRA_ISSUE_ID_TEST, properties.jiraTransitionInProgress());
    }

    final var issues = connector.getOpenIssues();
    assertNotNull(issues, "issues null");
    final var il = issues.toList();

    // check json parsing.
    assertFalse(il.isEmpty(), "no issues read: " + il.size());

    final var i = il.getFirst();
    assertNotNull(i.key(), "issue.key");
    assertNotNull(i.fields(), "issue.fields");
    assertNotNull(i.fields().created(), "issue.fields.created");
    assertTrue(i.fields().created().after(startDate), "issue.fields.created not recent");
    assertNotNull(i.fields().status().name(), "issue.fields.status");
  }

  @Test
  public void getTicketFromJira() {

    final var issue = connector.getIssue(JIRA_ISSUE_ID_TEST);
    assertNotNull(issue, "issues null");

    assertNotNull(issue.key(), "issue.key");
    assertNotNull(issue.fields(), "issue.fields");
    assertNotNull(issue.fields().comment(), "issue.fields.comment");
  }

  @Test
  public void changeTicketStateInJira() {

    final var issue = Require.notNull(connector.getIssue(JIRA_ISSUE_ID_TEST), "issue");

    final var oldState = issue.fields().status().id();

    final var targetTransition = oldState.equals(JiraStatus.STATUS_ID_IN_ARBEIT)
      ? properties.jiraTransitionRequestRun()
      : properties.jiraTransitionInProgress();

    final var targetState = oldState.equals(JiraStatus.STATUS_ID_IN_ARBEIT)
      ? JiraStatus.STATUS_ID_FERTIG
      : JiraStatus.STATUS_ID_IN_ARBEIT;

    connector.changeJiraIssueState(JIRA_ISSUE_ID_TEST, targetTransition);

    final var changedIssue = connector.getIssue(JIRA_ISSUE_ID_TEST);

    assertNotEquals(oldState, changedIssue.fields().status().id(), "issue state not changed.");
    assertEquals(targetState, changedIssue.fields().status().id(), "issue state unexpected.");
  }

  @Test
  public void changeTicketStateInvalidInJiraFails() {

    final var issue = connector.getIssue(JIRA_ISSUE_ID_TEST);

    final var oldState = issue.fields().status().id();

    Assumptions.assumeFalse(oldState.equals(JiraStatus.STATUS_ID_IN_ARBEIT),
      "Inconclusive, cannot be tested in current state " + oldState);

    Exception exception = assertThrows(HttpClientErrorException.class,
      () -> connector.changeJiraIssueState(JIRA_ISSUE_ID_TEST, properties.jiraTransitionRequestRun()));
    assertTrue(exception.getMessage().contains("Bad Request"), "400 Bad Request expected");

  }

  @Test
  public void addTicketCommentInJira() {

    final var newComment = "junit-comment-" + UUID.randomUUID();
    connector.addJiraIssueComment(JIRA_ISSUE_ID_TEST, newComment);

    final var issue = connector.getIssue(JIRA_ISSUE_ID_TEST);

    var found = Arrays.stream(issue.fields().comment().comments()).anyMatch(c -> c.body().equals(newComment));
    assertTrue(found, "comment " + newComment + " not found in " + issue.fields().comment());
  }

  @Test
  public void addTicketCommentWithEscapingInJira() {

    final var uuid = UUID.randomUUID().toString();
    final var newComment = "junit-comment-\":\\" + uuid;
    connector.addJiraIssueComment(JIRA_ISSUE_ID_TEST, newComment);

    final var issue = connector.getIssue(JIRA_ISSUE_ID_TEST);

    var found = Arrays.stream(issue.fields().comment().comments()).anyMatch(c -> c.body().contains(uuid));
    assertTrue(found, "comment " + newComment + " not found in " + issue.fields().comment());
  }

  @Test
  public void addTicketCommentToNonExistentInJiraFails() {

    Exception exception = assertThrows(HttpClientErrorException.class,
      () -> connector.addJiraIssueComment(JIRA_ISSUE_ID_TEST_NON_EXISTENT, "junit-comment-to-fail"));

    assertTrue(exception.getMessage().contains("Not Found"), "404 Not Found expected");
  }
}
