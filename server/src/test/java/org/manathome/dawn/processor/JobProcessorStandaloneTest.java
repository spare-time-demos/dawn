package org.manathome.dawn.processor;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.manathome.dawn.DawnJobProcessorTask;
import org.manathome.dawn.job.JobCommission;
import org.manathome.dawn.job.JobProcessingState;
import org.manathome.dawn.processor.jira.JiraConnector;
import org.manathome.dawn.processor.jira.JiraIssue;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.nio.file.Files;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * test file creation.
 */
@Tag("SystemTest")
@SpringBootTest
public class JobProcessorStandaloneTest {

  @Autowired
  JobProcessor processor;

  /** avoid changing non-existing jira tickets during this tests. */
  @MockBean
  JiraConnector mockConnector;

  /** avoid running background task. */
  @MockBean
  DawnJobProcessorTask stubbedOutTask;

  @BeforeEach
  public void beforeEach() {
    processor.cleanupControlFiles();
  }

  @Test
  public void scheduleJobOnDisk() {
    var j = new JobCommission("j1", "job1", JobProcessingState.commissioned);

    final var rc = processor.commission(j);

    assertTrue(rc, "job was commissioned");
    assertEquals(JobProcessingState.scheduled, j.getState(), "scheduled");
    assertTrue(Files.exists(processor.getJobFilePath(j)), "control file not created " + processor.getJobFilePath(j));
    assertEquals(1, processor.cleanupControlFiles(), "remove 1 created file.");
  }

  @Test
  public void retrieveOpenCommissions() {

    var issue = new JiraIssue("4711", "mock-job-1", null);
    Mockito.when(mockConnector.getOpenIssues()).thenReturn(Stream.of(issue));
    final var jcs = processor.retrieveOpenCommissions();

    Mockito.verify(mockConnector).getOpenIssues();
    assertTrue(!jcs.isEmpty());
    assertTrue(jcs.get(0).getId().equals("4711"));
  }
}
