@rem -- boostrap development environment for dawn
@rem -- -----------------------------------------

@echo single prerequisite: install chocolatey
@echo to be run with admin privileges

@echo install dev tools (java JDK + IDE) ...

choco install correttojdk
choco install intellijidea-community

choco upgrade correttojdk
choco upgrade intellijidea-community

